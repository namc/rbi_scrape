from django.conf.urls import include, url
from django.contrib import admin
from api import views, urls


urlpatterns = [
    # Examples:
    # url(r'^$', 'rbi.views.home', name='home'),
    url(r'', include(urls)),
    url(r'^admin/', include(admin.site.urls)),
]
