beautifulsoup4==4.5.1
bs4==0.0.1
Django==1.8
requests==2.11.1
wget==3.2
wheel==0.24.0
xlrd==1.0.0