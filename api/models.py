from django.db import models

# Create your models here.

class IfscData(models.Model):
	bank = models.CharField(max_length=255, blank=True)
	ifsc = models.CharField(max_length=255, blank=True)
	micr = models.CharField(max_length=255, blank=True)
	branch = models.CharField(max_length=255, blank=True)
	address = models.CharField(max_length=512, blank=True)
	contact = models.CharField(max_length=255, blank=True)
	city = models.CharField(max_length=255, blank=True)
	district = models.CharField(max_length=255, blank=True)
	state = models.CharField(max_length=255, blank=True)

	class Meta:
		ordering = ('id',)

					
