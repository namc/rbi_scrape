import os
import xlrd

from urllib import urlopen, urlretrieve
from bs4 import BeautifulSoup, SoupStrainer

from api.models import IfscData

def fetch():
	
	URL = 'https://www.rbi.org.in/Scripts/bs_viewcontent.aspx?Id=2009'
	OUTPUT_DIR = 'files/'  

	u = urlopen(URL)
	try:
		html = u.read().decode('utf-8')
	finally:
		u.close()

	soup = BeautifulSoup(html, "html.parser")
	for link in soup.select('a[href^="http://"]'):
		href = link.get('href')
		if not any(href.endswith(x) for x in ['.csv','.xls','.xlsx']):
			continue

		filename = os.path.join(OUTPUT_DIR, href.rsplit('/', 1)[-1])
		href = href.replace('http://','https://')

		print("Downloading %s" % (href) )
		urlretrieve(href, filename)


def parse():
	filesList = os.listdir('files/')

	for fileName in filesList :
		book = xlrd.open_workbook('files/' + fileName)
		sheet = book.sheet_by_index(0)

		rows = sheet.nrows
		cols = sheet.ncols

		for i in xrange(rows):

			if i != 0 :

				row_val = sheet.row_values(i)

				ifscObj = IfscData()
				ifscObj.bank = row_val[0]
				ifscObj.ifsc = row_val[1]
				ifscObj.micr = row_val[2]
				ifscObj.branch = row_val[3]
				ifscObj.address = row_val[4]
				ifscObj.contact = row_val[5]
				ifscObj.city = row_val[6]
				ifscObj.district = row_val[7]
				ifscObj.state = row_val[8]
				ifscObj.save()

				print "Saved"


