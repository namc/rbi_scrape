from django.core.management import BaseCommand

class Command(BaseCommand):
	help = 'Fetch data from RBI homepage and save to database'

	def handle(self, *args, **options):
		import api.utils as utils

		self.stdout.write("Fetching all data!")
		utils.fetch()
		utils.parse()
