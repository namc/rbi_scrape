from django.shortcuts import render, get_object_or_404
from api.models import IfscData
from django.http import HttpResponse
from django.forms.models import model_to_dict

import json

# Create your views here.

def getBankDataFromIfsc(request, ifsc):
	if request.method == 'GET' :

		try :
			bank_data = IfscData.objects.get(ifsc=ifsc)
			data_dict = model_to_dict(bank_data)
			if bank_data :
				return HttpResponse(json.dumps(data_dict), content_type='application/json; charset=UTF-8', status=200)

		except IfscData.DoesNotExist :
			message = "Data Does not exist"
			return HttpResponse(json.dumps(message), content_type='application/json')
	
			
def getBankData(request):
	if request.method == 'GET' :
		params = request.GET

		if 'name' in params :
			name = params['name']

			bank_data = IfscData.objects.filter(bank__contains=name)

			res = list()
			for data in bank_data :
				b = model_to_dict(data)
				res.append(b)

			if bank_data :
				return HttpResponse(json.dumps(res), content_type='application/json; charset=UTF-8', status=200)

			else :
				return HttpResponse(status=204)	
			


