from django.conf.urls import url
from api import views

urlpatterns = [
	url(r'^ifsc/(?P<ifsc>[A-Za-z0-9]{11})/$', views.getBankDataFromIfsc),
	url(r'^search$', views.getBankData, name='search'),
]